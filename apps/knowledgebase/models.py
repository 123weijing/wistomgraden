from django.db import models

#植物信息
class PlantInformation(models.Model):
    plantID = models.IntegerField(primary_key=True,verbose_name=u"植物ID")
    plantName=models.CharField(max_length=100,blank=False,verbose_name=u"植物名称")
    #植物图片类型未确定
    #plantPicture=models.CharField(verbose_name=u"植物图片")
    chineseName=models.CharField(max_length=100,blank=False,verbose_name=u"中文学名")
    nickName=models.CharField(max_length=100,blank=False,verbose_name=u"别称")
    subkingdom=models.CharField(max_length=100,blank=False,verbose_name=u"亚界")
    subdivision=models.CharField(max_length=100,blank=False,verbose_name=u"亚门")
    subclass=models.CharField(max_length=100,blank=False,verbose_name=u"亚纲")
    #字典
    plantType=models.CharField(max_length=100,verbose_name=u"植物类型")

    plantQRcode=models.CharField(max_length=100,verbose_name=u"植物二维码")
    latinName=models.CharField(max_length=100,blank=False,verbose_name=u"拉丁学名")
    binomialNomenclature=models.CharField(max_length=100,blank=False,verbose_name=u"二名法")
    kingdom=models.CharField(max_length=100,blank=False,verbose_name=u"界")
    phylum=models.CharField(max_length=100,blank=False,verbose_name=u"门")
    #outline是纲
    outline=models.CharField(max_length=100,blank=False,verbose_name=u"纲")
    note=models.CharField(max_length=200,verbose_name=u"备注")
#植物病虫害大全
class PlantDiseases(models.Model):
    pestID=models.IntegerField(primary_key=True,verbose_name=u"虫害ID")
    pestName=models.CharField(max_length=100,blank=False,verbose_name=u"名称")
    dstribution_And_hazard=models.CharField(max_length=100,verbose_name=u"分布与危害")
    mrphologicalCaracteristics=models.CharField(max_length=100,verbose_name=u"形态特征")
    generic=models.CharField(max_length=100,verbose_name=u"类属")
    harmGoal=models.CharField(max_length=100,verbose_name=u"危害目标")
    #未定图片类型
    #photograph=models.CharField()
    note=models.CharField(max_length=200,verbose_name=u"备注")
#虫害及防治
class PestControl(models.Model):
    ID=models.IntegerField(primary_key=True,verbose_name=u"ID")
    title=models.CharField(max_length=100,verbose_name=u"标题")
    content=models.CharField(max_length=100,verbose_name=u"内容")
    time=models.DateTimeField(verbose_name=u"时间")
    note=models.CharField(max_length=200,verbose_name=u"备注")
#法律法规
class LawsaAndRegulations(models.Model):
    ID=models.IntegerField(primary_key=True,verbose_name=u"ID")
    #字典
    regulationsType=models.CharField(max_length=100,verbose_name=u"法规类型")
    regulationsArea=models.CharField(max_length=100,verbose_name=u"法规区域")


    regulationsTitle=models.CharField(max_length=100,verbose_name=u"法规标题")
    note=models.CharField(max_length=200,verbose_name=u"备注")



