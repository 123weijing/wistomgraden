from django.apps import AppConfig


class KnowledgebaseConfig(AppConfig):
    name = 'knowledgebase'
    verbose_name = u"知识库"

